package ru.sfu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class SpringRun {
    @Autowired
    private static MusicalInstrumentRepository musicalInstrumentRepository;
    public SpringRun(MusicalInstrumentRepository studentRepository){
        this.musicalInstrumentRepository = studentRepository;
    }

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        MusicalInstrument mi = new MusicalInstrument(null, "1", "2", "3", 3.0, 0.25);
        ArrayList<MusicalInstrument> musicalInstruments = new ArrayList<MusicalInstrument>();
        musicalInstruments.add(mi);
        Optional<MusicalInstrument> m = musicalInstrumentRepository.findById(1);
        musicalInstrumentRepository.saveAll(musicalInstruments);
        musicalInstrumentRepository.save(mi);
    }
}
