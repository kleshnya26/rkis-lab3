package ru.sfu;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Table
@Entity
public class MusicalInstrument {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private String manufacturer;
    @Column
    private String model;
    @Column
    private Double price;
    @Column
    private Double possibleSale;

    public MusicalInstrument(Integer id, String name, String manufacturer, String model, Double price, Double possibleSale) {
        this.id = id;
        this.name = name;
        this.manufacturer = manufacturer;
        this.model = model;
        this.price = price;
        this.possibleSale = possibleSale;
    }

    public MusicalInstrument() {}

    @Override
    public String toString() {
        return "ru.sfu.MusicalInstrument{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", possibleSale=" + possibleSale +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPossibleSale() {
        return possibleSale;
    }

    public void setPossibleSale(Double possibleSale) {
        this.possibleSale = possibleSale;
    }
}
